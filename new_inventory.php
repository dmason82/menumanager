<html>
<!--
Copyright 2013 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<head><title>New inventory item</title></head>
<body>
<form action="add_pantry.php" method="post">
Quantity:
<input type = "text" name="amount" /> Ingredient:  <input type = text name="ingredient" />
<br />
<input type="submit">	
</form>
</body>
</html>