<!--view_pantry.php - views food inventory.
Copyright 2013 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head><title>Pantry</title></head>
<body>
<a href="new_inventory.php">Add item to inventory</a><br />
<table>
<tr><th>Quantity</th><th>Name</th></tr>
<?php
$dbhost = 'insert host name here';
$dbname = 'insert database instance here';
$dbuser = 'insert username here';
$dbpass = 'insert password here';
$mysql_handle = new mysqli($dbhost, $dbuser, $dbpass,$dbname) or die("Error connecting to database server");

$query = $mysql_handle->query("select sum(iv.quantity) as quantity,i.name as name from inventory iv inner join ingredient i on i.id = iv.ingredient_id group by i.id");
if($query){
	//$query->bind_result($quantity,$name);
	while ($row = $query->fetch_assoc()){
	echo "<tr><td>".$row['quantity']."</td><td>".$row['name']."</td></tr>";
	}

}
else{
echo $mysql_handle->error;
}
$mysql_handle->close();
?>
</table>
</body>

</html>