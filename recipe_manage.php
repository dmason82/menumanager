<!--recipe_manage.php - View of all current recipes in the database for meal managment application and links to the ability to add new recipes as well.
Copyright 2013 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
	<title>Recipe Management</title>
	</head>
	<style type="text/css" media="all">
	a.breadcrumb{
		text-align:right;
		color: 'orange';
	}
	table.recipes{
	margin-left: 15%;
	margin-right:auto;
	border: 1px solid black;
	border-bottom: 1px solid black;
		
}
	</style>
	<body>
	<a href="index.php" class="breadcrumb">Back to beginning.</a>
	<br />
	<a href="new_recipe.php">New Recipe</a>
	<table class="recipes">
	<?php
	ini_set('display_errors','1');
	/* MySQL connection bootstrap, 
	*/
	$dbhost = 'insert host name here';
	$dbname = 'insert database instance here';
	$dbuser = 'insert username here';
	$dbpass = 'insert password here';
	$mysql_handle = new mysqli($dbhost, $dbuser, $dbpass,$dbname) or die("Error connecting to database server");
	$recipes = $mysql_handle->query("SELECT id,name FROM dish");
	while($dish = $recipes->fetch_object()){
		echo "<tr><td><a href='view_recipe.php?id=$dish->id'>".$dish->name."</a></td><td><a href='add_shopping_list.php?id=$dish->id'>add to shopping list</a><td><a href='delete_recipe.php?id=$dish->id'>delete</a></td></tr>";
			
	}
	$mysql_handle->close();
	?>
	</table>
	</body>


	</html>