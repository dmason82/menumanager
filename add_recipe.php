<!--add_recipe.php - function for creating new recipes, including required ingredients and entries into dish_ingredient table
Copyright 2013 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>Adding new recipe...</head>
<body>
<?php
if($_POST['name']){
	$name = $_POST['name'];
}
else{
	echo "A name is required for entering a recipe";
	exit;
}
if($_POST['ingredients']){
$ingredients = $_POST['ingredients'];
}
else{
	echo "A list of ingredients is required.";
	exit;
}
if($_POST['servings']){
$servings = intval($_POST['servings']);
}
if($_POST['preparation']){
$preparation = $_POST['preparation'];
}
else{
	echo "Steps to prepare the recipe is required for entry.";
	exit;
}
/* MySQL connection bootstrap
*/
$dbhost = 'insert host name here';
$dbname = 'insert database instance here';
$dbuser = 'insert username here';
$dbpass = 'insert password here';
$mysql_handle = new mysqli($dbhost, $dbuser, $dbpass,$dbname) or die("Error connecting to database server");
$amounts = array();
$items = array();
$num = float;
$val = string;
echo "$name <br />";
$all_ingredients = explode("\n", $ingredients);


//Add ingredient to ingredients table to Databse before doing anything else.
if($preparedish = $mysql_handle->prepare("insert into dish (`name`,`servings`,`directions`) VALUES (?,?,?)")){
	$preparedish->bind_param("sis",$name,$servings,$preparation);
	$preparedish->execute();
}
$dishrow = $mysql_handle->insert_id;
if( $dishrow <= 0){
	echo "Unable to insert dish!";
	echo "<br /> $mysql_handle->error";
	exit;
}

	//print_r(explode("\n",$ingredients));
foreach ($all_ingredients as $item) {
	//Using the preg_split function in order to split out my units of measurement from my actual ingredients for form validation.
	
	$x = preg_split("@\|@", $item,NULL,PREG_SPLIT_NO_EMPTY);
	if($x){
		print_r($x);
		//echo "Eploded first element: $x[0]";
		$num = floatval($x[0]);
		$val = $x[1];
		if(!is_float($num)){
			echo "You need the ingredients to be a floating point number, indicating the amount you need in ounces.< br />";
			echo "Input format per incredient [float for oz]|Ingredient as string(The pipe is used as a delimiter for our units from ingredients.)";
			exit;
		}
		else{
			$amounts[] = $num;
		}
		if(!is_string($val)){
			echo "Erroneous string: $val <br />";
			echo "The ingredient needs to be inputted as a string.";
			echo "Input format per incredient [float for oz]|Ingredient as string(The pipe is used as a delimiter for our units from ingredients.)";
			exit;
		}
		else{
			$items[] = $val;
		}
	}
}
//Basic algorithm is that hopefully at this point I can check to see if the ingredient exists already.
//If it does, then I will retreive it's ID and use that for the foreign key, if not, I will need to insert a new 
//Ingredient, and then link to the resulting row.
for($i = 0; $i < count($amounts);$i++){
	$amount = $amounts[$i];
	$ingredient = $items[$i];
	echo "$amount $ingredient <br />";
	if($preparedingredient = $mysql_handle->prepare("select id,name from ingredient where name = ?")){
		$preparedingredient->bind_param("s",$ingredient);
		$preparedingredient->execute();
		$preparedingredient->bind_result($ingredientrow,$ingredientname);
		if($preparedingredient->fetch()){
			$preparedingredient->close;
			$prepareddish_ingredient = $mysql_handle->prepare("insert into dish_ingredient (`quantity`,`dish_id`,`ingredient_id`) VALUES (?,?,?)");
			$prepareddish_ingredient->bind_param("dis",$amount,$dishrow,$ingredientrow);
			$prepareddish_ingredient->execute();
			$prepareddish_ingredient->close();
			$dishingrow = $mysql_handle->insert_id;
			if($dishingrow <=0){
				echo "Unable to add ingredient $resultname into recipe."; 
				echo $mysql->error;
			}
			else{
					echo "inserted ingredient $dishingrow $ingredient";
				}		
		}
		else{
							
	//TODO: insert new ingredient, get its rowid, then insert a new DISH_INGREDIENT with the foreign key for ingredient pointing to it.
				$preparedingredient->close();
				if($preparedingredientinsert = $mysql_handle->prepare("insert into ingredient (`name`) VALUES ("."?".")")){
					$preparedingredientinsert->bind_param("s",$ingredient);
					$preparedingredientinsert->execute();
					$preparedingredientinsert->close();
					$ingredientrow = $mysql_handle->insert_id;
					echo $mysql_handle->insert_id."<br />";
					if($ingredientrow > 0){
						$prepareddish_ingredient = $mysql_handle->prepare("insert into dish_ingredient (`quantity`,`dish_id`,`ingredient_id`) VALUES (?,?,?)");
						$prepareddish_ingredient->bind_param("dis",$amount,$dishrow,$ingredientrow);
						$prepareddish_ingredient->execute();
						$prepareddish_ingredient->close();
						$dishingrow = $mysql_handle->insert_id;
						if($dishingrow <=0){
							echo "unable to insert";
							echo $mysql_handle->error;				
							}
						else{
							echo "Inserted".$dishingrow." ".$ingredient."<br />";
						}
					}
					else{
						echo "Unable to insert ingredient.";
						}
					}
				
				}
			}
		else{
			echo "Unable to prepare statement to fetch ingredient.</br>";
			exit;

	}
}
$mysql_handle->close();
?>
Recipe added. <a href="javascript:history.back()">Back to recipe list</a>
</body>
</html>