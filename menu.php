<!--menu.php - all menu functions are here for viewing at least
Copyright 2013 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<html>
<head>
</head>
	<style type="text/css" media="all">
	table {
		border: 2px solid #668;
		font-size:16pt;
		margin:0 auto;
	}
	
	td {
		padding: 0.1em 2em;
		border: 1px solid;
		border-spacing: 1em;
	}
	</style>
<body>
<table>
<tr><th>Date</th><th>meal</th><th>dish</th></tr>
<?php
/*MySQL Connection
*/
error_reporting(E_ALL);
$dbhost = 'insert host name here';
$dbname = 'insert database instance here';
$dbuser = 'insert username here';
$dbpass = 'insert password here';
$handle = new mysqli($dbhost,$dbuser,$dbpass,$dbname) or die ("Unable to connect to database");
$query = $handle->query("SELECT m.date AS day,m.name AS name ,d.name AS dish FROM dish_meal md INNER JOIN meal m ON m.id = md.meal_id INNER JOIN  dish d on d.id = md.dish_id");
if($query){
while($row = $query->fetch_assoc()){
echo "<tr><td>".$row['day']."</td><td>".$row['name']."</td><td>".$row['dish']."</td></tr>";
}
}
else{
echo $mysqli->error;
}
$handle->close();
?>
</body>
</html>