Menumanager
A PHP site to manage meals. As of note, while it does use the PHP mysqli class you will need to create a mysql database instance,create a username and password for authorization into the database with read and insert access and , then create the tables within your database using the following SQL code prior to running the site:

DROP TABLE IF EXISTS `dish_ingredient`;
DROP TABLE IF EXISTS `shoppinglist`;
DROP TABLE IF EXISTS `dish_meal`;
DROP TABLE IF EXISTS `ingredient`;
DROP TABLE IF EXISTS `meal`;
DROP TABLE IF EXISTS `dish`;
CREATE TABLE `ingredient` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(255) NOT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB;
CREATE TABLE `dish`(`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(255) NOT NULL, `servings` int(11),
`directions` varchar(2048) NOT NULL, PRIMARY KEY (`id`)
) ENGINE=InnoDB;
CREATE TABLE `meal`(
`id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL,
`date` date NOT NULL,
PRIMARY KEY (`id`)
)ENGINE=InnoDB;
CREATE TABLE `inventory`(
`id` int(11) NOT NULL AUTO_INCREMENT,
`quantity` int(11) NOT NULL,
`ingredient_id` int(11) NOT NULL,
PRIMARY KEY (`id`),
FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient`(`id`) )ENGINE=InnoDB;
CREATE TABLE `shoppinglist`(
`id` int(11) NOT NULL AUTO_INCREMENT,
`quantity` float NOT NULL,
`ingredient_id` int(11) NOT NULL,
`purchased` int(11) ,
PRIMARY KEY(`id`),
FOREIGN KEY (`ingredient_id`) references `ingredient`(`id`) ) ENGINE=InnoDB;
CREATE TABLE `dish_meal` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`dish_id` int(11) NOT NULL,
`meal_id` int(11) NOT NULL,
PRIMARY KEY (`id`),
FOREIGN KEY (`dish_id`) REFERENCES `dish`(`id`), FOREIGN KEY (`meal_id`) REFERENCES `meal`(`id`)
)Engine=InnoDB;
CREATE TABLE dish_ingredient(
`id`int(11) NOT NULL AUTO_INCREMENT, `dish_id` int(11) NOT NULL,
`ingredient_id` int(11) NOT NULL,
`quantity` float NOT NULL,
PRIMARY KEY (`id`),
FOREIGN KEY (`dish_id`) REFERENCES `dish`(`id`),
FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient`(`id`)
) ENGINE=InnoDB;