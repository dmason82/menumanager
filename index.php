<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<!--
index.php - Beginning for our site to bring the user into the different aspects of the meal tracking application.
Copyright 2013 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
	<head>
	<title>Meal Manager - by Douglas Mason</title>
	</head>
	<style type="text/css" media="all">
	table {
		border: 2px solid #668;
		font-size:16pt;
		margin:0 auto;
	}
	
	td {
		padding: 0.1em 2em;
		border: 1px solid;
		border-spacing: 1em;
	}
	</style>
</head>
<table>
	<tr>
		<td>
			<a href ="recipe_manage.php">Recipe Management</a>
		</td>
	</tr>
		<tr>
		<td><a href="view_pantry.php">Pantry</a>
		</td>
	</tr>
		<tr>

		<td>
			<a href="menu.php">Menu</a>
		</td>
	</tr>
		<tr>
		<td><a href="view_shopping_list.php">Shopping List</a>
		</td>
	</tr>
</table>
</html>