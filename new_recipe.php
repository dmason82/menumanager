<!--new_recipe.php
Copyright 2013 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<title>New Recipe</title>
</head>
<body>
New recipe: In order to add a new recipe, there needs to be a name, there may be a number of servings, ingredients, and preparation notes. 
For your ingredients they need to be entered in the form of a decimal amount ex. 25.2|ingredient name , the vertical | delineates between the ingredient amount and what the ingredient is.
<form action="add_recipe.php" method="post">
	Name of dish: <input type="text" name="name">
	<br />
	Servings:
<input type = "text" name="servings">
<br />
	Ingredients:
<br /> 

<textarea name="ingredients" rows="15" ></textarea>
	<br />
	Preparation: <br />
	<textarea name="preparation" rows="15" ></textarea>
	<br />
	<input type="submit">	
</form>
<body>
</html>