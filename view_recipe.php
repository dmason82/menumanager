<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<!--
Copyright 2013 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<HTML>
<style>
p.header{
font-size: 18;
font-weight: bold;
}
span.title{
font-size: 30;
font-style: italic;}
</style>
<?php
ini_set('display_errors','1');
$dbhost = 'insert host name here';
$dbname = 'insert database instance here';
$dbuser = 'insert username here';
$dbpass = 'insert password here';
if(! is_null($_GET['id'])){
$parameter = $_GET['id'];
}
else{
$parameter="1";
}

$mysql_handle = new mysqli($dbhost, $dbuser, $dbpass,$dbname) or die("Error connecting to database server");

//Fetch the information for the recipe from the dish table and bind those results to variables for presenting later.
if($preparedstatement= $mysql_handle->prepare("SELECT name,directions from dish where id=?")){
$preparedstatement->bind_param('s',$parameter);
$preparedstatement->execute();
$preparedstatement->bind_result($result,$result2);
$preparedstatement->fetch();
$preparedstatement->close();
echo "Recipe information for : <span class=title>";
echo $result;
echo "</span>";
echo "<br />";

//Fetch information from the joined query from the dish_ingredient table and present the relevant data to the screen.
if($preparedingredients = $mysql_handle->prepare("SELECT tmp.amount AS amount, tmp.name AS name FROM ( SELECT di.quantity AS amount, i.name AS name, d.name AS dish FROM dish_ingredient di INNER JOIN ingredient i ON di.ingredient_id = i.id INNER JOIN dish d ON di.dish_id = d.id) AS tmp WHERE tmp.dish =?")){
$preparedingredients->bind_param("s",$result);
$params = array();
$ingred = array();
$preparedingredients->execute();
$meta = $preparedingredients->result_metadata();
while ($field = $meta->fetch_field()){
$params[] = &$row[$field->name];
}
call_user_func_array(array($preparedingredients,'bind_result') , $params);
while($preparedingredients->fetch()){
$x = array();
foreach ($row as $key => $value) {
	# code...
$x[$key]=$value;
}
$ingred[] = $x;
}

foreach ($ingred as $row) {
	echo $row['amount']." ".$row['name'];
	echo "<br />";
	# code...
}
}
else{
echo "Cannot fetch ingredients!";
}

//Print out directions for recipe

echo "<p class='header'>Directions: </p>";
echo $result2;
}
else{
echo $mysql_handle->error;		
}
$mysql_handle->close();
?>
</HTML>