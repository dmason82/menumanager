<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<!--
Copyright 2013 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<head><title>Adding recipe to shopping list</title></head>
<body>
<?php
$dbhost = 'insert host name here';
$dbname = 'insert database instance here';
$dbuser = 'insert username here';
$dbpass = 'insert password here';
$mysql_handle = new mysqli($dbhost, $dbuser, $dbpass,$dbname) or die("Error connecting to database server");
$ingredient_amounts = array();
$ingredient_names = array();
if($_GET['id']){
$id = intval($_GET['id']);
}
else{
echo "Need to know which recipe to add to the list!";
exit;
}
if($ingredientsquery = $mysql_handle->prepare("select quantity,ingredient_id from dish_ingredient where dish_id = ?")){
$ingredientsquery->bind_param("i",$id);
$ingredientsquery->execute();
mysqli_bind_result($ingredientsquery,$quantity,$ingredient_id);
 while($row = $ingredientsquery->fetch()){
 $ingredient_amounts[]=$quantity;
 $ingredient_names[]=$ingredient_id;
 }
 $ingredientsquery->close();
 //Insert each ingredient into the shoppinglist using a prepared SQL query.
for ($i = 0; $i < count($ingredient_amounts);$i++) {
		echo "<br />";
		if($insertquery = $mysql_handle->prepare("insert into shoppinglist (`quantity`,`ingredient_id`) VALUES ("."?,?".")")){
		$insertquery->bind_param("di",$ingredient_amounts[$i],$ingredient_names[$i]);
		$insertquery->execute();
		$insertquery->close();
		}
}
}
else{
echo $mysql_handle->error;
}
$mysql_handle->close();
?>
Successfully added. <a href="recipe_manage.php">Back to recipe list</a>
</body>
</html>