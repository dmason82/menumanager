<!--
delete_recipe.php - it deletes from the dishes table , and anything that hangs off of it, since we would need to remove from the dish_ingredient first due to the foreign key constraint.
Copyright 2013 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
</head>
<body>
<?php
if(! is_null($_GET['id'])){
$parameter = intval($_GET['id']);
}
else{
echo "Cannot delete a dish if I don't know what it is."
}
	/* MySQL connection bootstrap, 
	*/
	$dbhost = 'insert host name here';
	$dbname = 'insert database instance here';
	$dbuser = 'insert username here';
	$dbpass = 'insert password here';
	$mysql_handle = new mysqli($dbhost, $dbuser, $dbpass,$dbname) or die("Error connecting to database server");
	
	if($preparedstatement = $mysqli->prepare("delete * from dish_ingredient where dish_id = ?")){
		$preparedstatement->bind_param("i",$parameter);
		$preparedstatement->execute();	
}
	if($query = $mysqli->query("delete from dish where id = $parameter")){
	}
?>
</body>
</html>