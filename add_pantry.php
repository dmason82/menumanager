<!--add_pantry.php - adds ingredients into the pantry based on input from a form.
Copyright 2013 Douglas Mason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head><title>Adding ingredient to inventory</title></head>
<body>
<?php
$dbhost = 'insert host name here';
$dbname = 'insert database instance here';
$dbuser = 'insert username here';
$dbpass = 'insert password here';
$mysql_handle = new mysqli($dbhost, $dbuser, $dbpass,$dbname) or die("Error connecting to database server");
if($_POST['ingredient']){
$id = $_POST['ingredient'];
}
else{
echo "Need to know which ingredient to add!";
exit;
}
if($_POST['amount']){
$amount =floatval($_POST['amount']);
}
else{
"Need to know how much of an ingredient you have in stock";
}
if($insertquery = $mysql_handle->query("insert into inventory (`quantity`,`ingredient_id`) VALUES ($amount,("."select id from ingredient where name = '$id'"."))")){
}
else{
echo $mysql_handle->error;
}
$mysql_handle->close();
?>
Successfully added. <a href="view_pantry.php">Back to pantry.</a>
</body>
</html>